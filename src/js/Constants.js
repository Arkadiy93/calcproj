export const buttons = {
    numerical : [1,2,3,4,5,6,7,8,9,".",0,"DEL"],
    symbols : ["/","x","-","+"],
    panel : ["INV","DEG","&#37;","sin","cos","tan","ln","log","!","&‌prod;","e","^","(",")","&‌radic;"]
}

