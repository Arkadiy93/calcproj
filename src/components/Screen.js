import React from 'react';
import '../css/Screen.css';
import TypingScreen from './TypingScreen';
import ResultScreen from './ResultScreen';

function Screen(props) {

    return(
        <div className="Screen">
            <TypingScreen >{props['data-typing-screen']}</TypingScreen>
            <ResultScreen > {props['data-result-screen']} </ResultScreen>
        </div>
    )
}

export default Screen;