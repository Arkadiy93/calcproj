import React from 'react';
import '../css/TypingScreen.css';

function TypingScreen(props) {
    return <div {...props} className="TypingScreen">
        {props.children}
    </div>

}

export default TypingScreen;