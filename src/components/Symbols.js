import React from 'react';
import '../css/Symbols.css';
import {buttons} from '../js/Constants';
import Button from './Button';

function Symbols() {
    const buttonProps = {
        'data-button-type':"symbols",
        className : "symbolButtons"
    };
    return(
        <div className="Symbols">{
            buttons.symbols.map((char) =>
                <Button {...buttonProps} key = {char} >{char}</Button>
            )
        }
        </div>
    )
}

export default Symbols;