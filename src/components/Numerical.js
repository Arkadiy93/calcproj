import React from 'react';
import '../css/Numerical.css';
import {buttons} from '../js/Constants';
import Button from './Button';

function Numerical() {
    const buttonProps = {
        'data-button-type':"Numerical",
        className : "numericalButtons"
    };

    return(
        <div className="Numerical">{
            buttons.numerical.map((numerical) =>{
                    return (
                        <Button {...buttonProps} key = {numerical}>
                            {numerical}
                        </Button>)
                }
            )
        }
        </div>
    )
}

export default Numerical;
