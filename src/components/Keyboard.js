import React from 'react';
import '../css/Keyboard.css';
import Numerical from './Numerical';
import Symbols from './Symbols';
import SubPanel from './SubPanel';

function Keyboard() {
    return(
        <div className="Keyboard">
            <Numerical />
            <Symbols />
            <SubPanel />
        </div>
    )
}

export default Keyboard;