import React from 'react';
import '../css/ResultScreen.css';

function ResultScreen(props) {
    return <div className="ResultScreen">
        {props.children}
        </div>
}

export default ResultScreen;