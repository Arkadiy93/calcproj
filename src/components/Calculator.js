import React, { Component } from 'react';
import '../css/Calculator.css';
import Menu from './Menu.js';
import Screen from './Screen.js';
import Keyboard from './Keyboard.js';
import * as Methods from '../js/Methods';
import R from "../js/ramda"

class Calculator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            history: [],
            resultScreen: "",
            typingScreen: "",
            startCalculate: false, // state that either allows to start automatic calculation either not.
            resultGiven : false // determines if the result was already given.It's needed to control the input after the result was given.
        };
    }


    startCalculate(variable){
        if(variable){
            return true;
        }else{

            return this.state.startCalculate;
        }
    }

    pickButton(event,variables){
        if(event.nativeEvent.target.innerHTML != "DEL"  ){
            variables.value = event.nativeEvent.target.innerHTML;
            if(this.state.resultGiven){
                if(variables.className == "Symbols")return;
                variables.TypingScreen = variables.value;
            }else{
                variables.TypingScreen = this.state.typingScreen + "" + variables.value;
            }
        }
    }

    symbolCheck(event,variables){

        // confirming that the first input cannot be a symbol either at there cannot be 2 symbols in a row

        if (isNaN(variables.value)  && event.nativeEvent.target.innerHTML != "DEL" ){
            if (this.state.typingScreen.length == 0 || isNaN(this.state.typingScreen.slice(-1))){
                return true;
            }
        }
    }

    autoCalculation(variables){

        //here we allow automatic calculation to start since a symbol was pressed

        if(variables.className == "Symbols"  && (variables.value != "DEL")){
            variables.startCalculate = true;
        }
    }

    calculation(variables){

        if(variables.startCalculate == true || this.state.startCalculate == true){
            try {
                variables.ResultScreen = eval(variables.TypingScreen.replace(/x/g, "*"))
            } catch (err) {}
        }
    }

    deleteButton(variables){
        let {state} = this;
        let temp = "";
        if(isNaN(this.state.typingScreen)){
            if(isNaN(this.state.typingScreen.slice(-1)) && isNaN(this.state.typingScreen.slice(0,-1)) == false){
                //DO NOTHING
            }else{
                try {
                    temp = eval(this.state.typingScreen.slice(0,-1).replace(/x/g, "*"))
                } catch (err) {}
            }
        }
        this.setState(R.pipe(
            R.assocPath(["typingScreen"], this.state.typingScreen.slice(0,-1)),
            R.assocPath(["resultScreen"], temp),
            R.assocPath(["startCalculate"],isNaN(this.state.typingScreen) ? true : false)
        )(state))
    }

    settingStates(variables){
        let {state} = this;
        this.setState(R.pipe(
            R.assocPath(["typingScreen"], variables.TypingScreen),
            R.assocPath(["resultScreen"], variables.ResultScreen),
            R.assocPath(["startCalculate"], this.startCalculate(variables.startCalculate)),
            R.assocPath(["resultGiven"], false)
        )(state))
    }

    panelButton(event){

        if(event.target.className == "SubPanel")return true;
    }

    buttonPress(event) {
        let variables = {
            value : "",
            TypingScreen : "",
            ResultScreen : "",
            className : event.nativeEvent.target.parentElement.className,
            startCalculate : ""
        };
        console.log(event)
        if(this.panelButton(event))return;
        this.pickButton(event, variables);
        if(this.symbolCheck(event, variables))return;
        this.autoCalculation(variables);
        this.calculation(variables);

        if(event.nativeEvent.target.innerHTML == "DEL"){
            this.deleteButton(variables);
        }else{
            this.settingStates(variables)
        }
    }

    render() {

        const screenProps = {
            'data-result-screen': this.state.resultScreen,
            'data-typing-screen' : this.state.typingScreen

        }

        return (
            <div id="main" onClick={(event)=>{this.buttonPress(event)}}>
                <Menu/>
                <Screen {...screenProps} />
                <Keyboard/>
            </div>
        )
    }
}

export default Calculator;
